# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bluedevil
pkgver=5.25.2
pkgrel=0
pkgdesc="Integrate the Bluetooth technology within KDE workspace and applications"
# armhf blocked by qt5-qtdeclarative
# armhf, s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends="
	bluez
	bluez-obexd
	kded
	"
makedepends="
	bluez-qt-dev
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	kded
	kded-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	shared-mime-info
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/bluedevil-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
270ac2f073b1ea223b3500bb99fa5fcacfd738305757b79904f4a82448322ed718210cd36a61900eea69b6ad9f778a56939e7eb27e5b13c9846ca29799f92d84  bluedevil-5.25.2.tar.xz
"

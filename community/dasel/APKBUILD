# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dasel
pkgver=1.25.0
pkgrel=0
pkgdesc="Query and modify data structures using selector strings"
url="https://daseldocs.tomwright.me/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/TomWright/dasel/archive/v$pkgver/dasel-$pkgver.tar.gz"

export GOPATH="$srcdir"
export GOFLAGS="$GOFLAGS -trimpath -modcacherw"
export CGO_ENABLED=0

build() {
	go build -ldflags "
		-X github.com/tomwright/dasel/internal.Version=$pkgver
		" ./cmd/dasel
}

check() {
	go test ./...
}

package() {
	install -Dm755 dasel -t "$pkgdir"/usr/bin/
}

sha512sums="
ec2b1d0ca0acd84dc9465b0f4aa27b054a0897efbcb4dd6a94cf6de73415933e44b857e8dbcb01d3bac266597d2dfe132146d282c168b087dcd77512e5f06e72  dasel-1.25.0.tar.gz
"

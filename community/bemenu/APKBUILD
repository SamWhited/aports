# Contributor: Cosmo Borsky <me@cosmoborsky.com>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bemenu
pkgver=0.6.9
pkgrel=0
pkgdesc="Dynamic menu library and client program with support for different backends"
options="!check" # No testsuite
url="https://github.com/Cloudef/bemenu"
arch="all"
license="GPL-3.0-or-later AND LGPL-3.0-or-later"
depends_dev="
	libxinerama-dev
	libxkbcommon-dev
	ncurses-dev
	pango-dev
	scdoc
	wayland-dev
	wayland-protocols
	"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
source="$pkgname-$pkgver.tar.gz::https://github.com/Cloudef/bemenu/archive/$pkgver.tar.gz"
replaces="sxmo-bemenu"

build() {
	PREFIX=/usr CFLAGS="$CFLAGS -g" make
}

package() {
	# Please don't split backends into subpackages until
	# https://github.com/Cloudef/bemenu/issues/165 is
	# resolved, i.e. proper error messages are output if
	# no backend was found or a backend error occured.

	make PREFIX=/usr DESTDIR="$pkgdir" install
}

sha512sums="
143c433eb6e30882f374afe0af0c0187690b9c39fc72c210a071b11fc26c0e51a5c2006858ec08ba63f7ceb33689f5b4b0b97ff365d2c108a2024b8eb4972bfd  bemenu-0.6.9.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze
pkgver=5.25.2
pkgrel=0
pkgdesc="Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends_dev="
	kconfigwidgets-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kirigami2-dev
	kpackage-dev
	kwindowsystem-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
78b0fb2c8817010f268f59e35c85b1698eefba154834ae6430564d2ef7ede078e5408ba787b52b40d4f841ec7b5e49114a42314d61f4cc93acb2066ea1bf324a  breeze-5.25.2.tar.xz
"

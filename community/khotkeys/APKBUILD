# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khotkeys
pkgver=5.25.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by libksysguard
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
pkgdesc="Key Accelerator Application"
license="GPL-2.0-only AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/khotkeys-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
021e548b590a9c1cc9ba65719fb118cc6c5e6902c428333d58ab79d62535d7317ebcc05760c45a95a9e1e064395802c8e225724e7e4601d3af9e4913e1c5694c  khotkeys-5.25.2.tar.xz
"

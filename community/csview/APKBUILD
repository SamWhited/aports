# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=csview
pkgver=1.0.1
pkgrel=0
pkgdesc="Pretty csv viewer for cli with cjk/emoji support"
url="https://github.com/wfxr/csview"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # blocked by rust/cargo
license="Apache-2.0 OR MIT"
makedepends="cargo"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/wfxr/csview/archive/v$pkgver/csview-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	install -D -m644 completions/bash/$pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname

	install -D -m644 completions/fish/* -t "$pkgdir"/usr/share/fish/completions/
	install -D -m644 completions/zsh/* -t "$pkgdir"/usr/share/zsh/site-functions/
}

sha512sums="
c1b0e5d4b97ba5078dda7145b2a130e0bf804332232c9b8625f003e72afe4b8aaebf724e0eeb3f462005dd7c06a28303f85e6714d20ba685e114c7e2edb05342  csview-1.0.1.tar.gz
"

# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=xdg-desktop-portal-kde
pkgver=5.25.2
pkgrel=0
pkgdesc="A backend implementation for xdg-desktop-portal that is using Qt/KDE"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !s390x !riscv64"
url="https://phabricator.kde.org/source/xdg-desktop-portal-kde"
license="LGPL-2.0-or-later"
depends="xdg-desktop-portal"
makedepends="
	cups-dev
	extra-cmake-modules
	glib-dev
	kcoreaddons-dev
	kdeclarative-dev
	kio-dev
	kirigami2-dev
	kwayland-dev
	libepoxy-dev
	pipewire-dev
	plasma-framework-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/xdg-desktop-portal-kde-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKDE_INSTALL_LIBEXECDIR=libexec
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0bc27ec697c99fd467c66baf7c0fc4cd2233052eeb49da355793606423851b4b98c3597ddb66e5cb3827b9d231355cfeec4017e3f47a785111f53a9677ac12ec  xdg-desktop-portal-kde-5.25.2.tar.xz
"

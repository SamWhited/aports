# Contributor: Newbyte <newbie13xd@gmail.com>
# Maintainer: Newbyte <newbie13xd@gmail.com>
pkgname=karlender
pkgver=0.5.1
pkgrel=0
pkgdesc="Mobile friendly GTK based calendar app written in Rust"
url="https://gitlab.com/loers/karlender"
# ppc64le: build script for dependency ring 0.16.20 fails
# s390x, riscv64: blocked by rust/cargo
arch="all !ppc64le !riscv64 !s390x"
license="GPL-3.0-or-later"
makedepends="
	cargo
	cargo-gra
	libadwaita-dev
	"
source="https://gitlab.com/loers/karlender/-/archive/v$pkgver/karlender-v$pkgver.tar.gz
	"
options="!check"
builddir="$srcdir/$pkgname-v$pkgver"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo gra gen
	cargo build --release --frozen
}

package() {
	cargo install --locked --root "$pkgdir/usr" --path .
	make -C target/gra-gen install ROOT="$pkgdir/usr"

	rm "$pkgdir"/usr/.crates.toml
	rm "$pkgdir"/usr/.crates2.json
}

sha512sums="
4d280e9777ef3a4679f0a7197f48cea6be7f2d40dd9dc24aa9a1425884f628dd218abfe0dc99bf118c750db9cc571e3f80a2d17b0e53c39f2717dbc775f63e46  karlender-v0.5.1.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-gtk
pkgver=5.25.2
pkgrel=0
pkgdesc="A GTK Theme Built to Match KDE's Breeze"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> breeze
arch="noarch !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only"
depends="gtk-engines"
makedepends="
	breeze
	breeze-dev
	extra-cmake-modules
	py3-cairo
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-gtk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
fbae160173b9b51ad7e4ce56850c0fa9734abe294a4a606880e2e7adf83e9053ca90f7848bf61a978df82d6463ed2d9bb8b8b1c47ed5ff9651643a65c9ee3d3a  breeze-gtk-5.25.2.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwallet-pam
pkgver=5.25.2
pkgrel=1
pkgdesc="KWallet PAM integration"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kwallet
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
depends="socat"
makedepends="
	extra-cmake-modules
	kwallet-dev
	libgcrypt-dev
	linux-pam-dev
	samurai
	socat
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwallet-pam-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/ \
		-DCMAKE_INSTALL_LIBEXECDIR=usr/libexec \
		-DCMAKE_INSTALL_LIBDIR=lib # for the pam module to be in /lib/security
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/lib/systemd
}

sha512sums="
58c145850a0adabe69875a62f2354d7dc4744d2ef30b05e011e168b75e260333cd4e3d8fa1ae48a1a541c121add22a34d87d6bb74780b6105c7bfa042e841a4b  kwallet-pam-5.25.2.tar.xz
"

# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=wolfssl
pkgver=5.3.0
_pkgver=$pkgver-stable
pkgrel=2
pkgdesc="Embedded TLS Library"
url="https://www.wolfssl.com"
arch="all"
license="GPL-2.0-only"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	autoconf
	automake
	libtool
	"
subpackages="$pkgname-dev $pkgname-fast"
source="https://github.com/wolfSSL/wolfssl/archive/v$_pkgver/wolfssl-$_pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"
options="!check"  # there are actually no tests! >_<

prepare() {
	default_prepare

	./autogen.sh

	cp -ar "$builddir" "$builddir~fast"
}

build() {
	_build

	cd "$builddir~fast"

	local extra_opts=
	case "$CARCH" in
		 x86_64) extra_opts="--enable-intelasm --enable-sp-asm";;
	esac
	CFLAGS="${CFLAGS/-Os/} -O3" _build --enable-sp $extra_opts
}

_build() {
	# Note: Primary development uses automake, the support for CMake is
	# still under development.

	# We use --enable-all and --enable-reproducible-build instead of
	# --enable-distro to ensure options.h gets installed.
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-silent-rules \
		--enable-shared \
		--enable-static \
		--enable-reproducible-build \
		--enable-all \
		--enable-sha512 \
		--enable-bigcache \
		--enable-base64encode \
		--disable-md5 \
		--enable-sni \
		--enable-pkcs11 \
		--disable-examples \
		"$@"
	make
}

package() {
	provider_priority=100  # highest (other provider is $pkgname-fast)

	make DESTDIR="$pkgdir" install

	# No useful stuff here.
	rm -rf "$pkgdir"/usr/share/doc
}

fast() {
	pkgdesc="$pkgdesc - optimized for performance"
	provider_priority=10  # lowest (other provider is $pkgname)
	options="!tracedeps"

	mkdir -p "$subpkgdir"/usr/lib
	cp -P "$builddir~fast"/src/.libs/libwolfssl.so.* "$subpkgdir"/usr/lib/
}

# XXX: Override this function from abuild to avoid unwanted dependency on
# wolfssl-fast in wolfssl-dev.
prepare_symlinks() {
	true
}

sha512sums="
399d2b8aad58471d237d21dea68c33fde2b9a3c117c554c241d9174db02847a6c31afae2908839d18f9ada317b2388560a24c077b76014f663227061342bf045  wolfssl-5.3.0-stable.tar.gz
"

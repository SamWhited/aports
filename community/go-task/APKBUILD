# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=go-task
pkgver=3.13.0
pkgrel=0
pkgdesc="Task runner written in Go, installed as go-task"
url="https://taskfile.dev/"
arch="all"
license="MIT"
makedepends="go"
subpackages="
	$pkgname-doc
	$pkgname-task::noarch
	$pkgname-bashcomp::noarch
	$pkgname-fishcomp::noarch
	$pkgname-zshcomp::noarch
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/go-task/task/archive/refs/tags/v$pkgver.tar.gz
	completion-rename-to-go-task.patch
	hide-signals-test-behind-build-tag.patch
	"
builddir="$srcdir/task-$pkgver"

export GOPATH="$srcdir/go"
export GOCACHE="$srcdir/go-build"
export GOTMPDIR="$srcdir"

export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -v -ldflags="-X main.version=$pkgver" github.com/go-task/task/v3/cmd/task
}

check() {
	go build -o bin/ ./cmd/sleepit
	go test -v ./...
}

package() {
	install -Dm0755 task "$pkgdir"/usr/bin/go-task
	install -Dm0644 docs/docs/usage.md "$pkgdir"/usr/share/doc/go-task/usage.md

	install -Dm0644 completion/bash/task.bash \
		"$pkgdir"/usr/share/bash-completion/completions/go-task.bash
	install -Dm0644 completion/fish/task.fish \
		"$pkgdir"/usr/share/fish/completions/go-task.fish
	install -Dm0644 completion/zsh/_task \
		"$pkgdir"/usr/share/zsh/site-functions/_go_task
}

task() {
	pkgdesc="Task runner, written in Go, installed as task"
	depends="!task"

	mkdir -p "$subpkgdir/usr/bin"
	ln -s /usr/bin/go-task "$subpkgdir/usr/bin/task"
}

sha512sums="
346b6767fa0396ed11f248b0c76773e460ec6fabf2491ff59739b3e6d87d2ae9e08d3dd40062ace4f472862059d8d7149e0334f9cbbc5fd48f75107884ecf5bf  go-task-3.13.0.tar.gz
ef909580b18341b6b7a5e21d3a8d3331f5a0de6dd05d2ba63b1f11c21dd54676eb277c9df88673fab1a654706dae3298f758e534e4237c42da7bcd6ac78d25cd  completion-rename-to-go-task.patch
f2bc6754721cb0cbc686809614e096d538e168a9f1c2173e42682c6d6240cf46098bd0a0d3368bc56a4aeefc3bb5b4f4532c740b4d0f9dde2997ea4afbfe68f2  hide-signals-test-behind-build-tag.patch
"
